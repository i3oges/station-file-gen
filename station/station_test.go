package station

import (
	"os"
	"reflect"
	"strings"
	"testing"
)

type Test struct {
	expected string
	actual   string
	name     string
}

func happyPath() Test {

	type ts struct {
		Name string `len:"4"`
	}

	station := ts{Name: "test"}
	rt := reflect.TypeOf(ts{})

	return Test{
		actual:   findKeyLen("Name", station.Name, rt),
		expected: "test",
		name:     "happy path",
	}
}

func extraLen() Test {
	type ts struct {
		Name string `len:"8"`
	}

	station := ts{Name: "test"}
	rt := reflect.TypeOf(ts{})

	return Test{
		actual:   findKeyLen("Name", station.Name, rt),
		expected: "test    ",
		name:     "extra length",
	}
}

func rightAlign() Test {
	type ts struct {
		Name string `len:"8" from:"right"`
	}
	station := ts{Name: "test"}
	rt := reflect.TypeOf(ts{})

	return Test{
		actual:   findKeyLen("Name", station.Name, rt),
		expected: "    test",
		name:     "right align",
	}
}

func noMetaData() Test {
	type ts struct {
		Name string
	}
	station := ts{Name: "test"}
	rt := reflect.TypeOf(ts{})

	return Test{
		actual:   findKeyLen("Name", station.Name, rt),
		expected: "",
		name:     "no metadata",
	}
}

func noData() Test {
	type ts struct {
		Name string
	}
	station := ts{Name: ""}
	rt := reflect.TypeOf(ts{})

	return Test{
		actual:   findKeyLen("Name", station.Name, rt),
		expected: "",
		name:     "no data",
	}
}

func TestFindKeyLen(t *testing.T) {
	tt := []Test{
		noData(),
		noMetaData(),
		happyPath(),
		extraLen(),
		rightAlign(),
	}

	for _, test := range tt {
		t.Run(test.name, func(t *testing.T) {
			if strings.Compare(test.expected, test.actual) == 1 {
				t.Errorf("expected %v to equal %v", test.expected, test.actual)
			}
		})
	}
}

func TestCreateDirectory(t *testing.T) {
	defer os.Remove("happy")
	err := createDirectory("happy")
	if err != nil {
		t.Fatalf("expected directory to be created: %v", err)
	}
}

func TestCreateDirectoryAlreadyExists(t *testing.T) {
	defer os.Remove("test")
	if err := os.Mkdir("test", os.ModePerm); err != nil {
		t.Fatalf("Couldn't create directory for test: %v", err)
	}
	if err := createDirectory("test"); err == nil {
		t.Fatalf("expected directory that already exists to cause an error: %v", err)
	}
}

func TestCreateStationFile(t *testing.T) {
	defer os.RemoveAll("test")
	CreateStationFile("test")

	if _, err := os.Stat("test/file.ctl"); os.IsNotExist(err) {
		t.Fatalf("expected test directory with file.ctl to be created: %v", err)
	}
}
