package station

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"strconv"
)

type station struct {
	MainID   string `len:"8"`
	Location string `len:"5" from:"right"`
}

func findKeyLen(key string, val string, t reflect.Type) string {
	k, _ := t.FieldByName(key)
	if kLen, ok := k.Tag.Lookup("len"); ok {
		length, err := strconv.Atoi(kLen)

		if err != nil {
			log.Fatalf("formatting %v: %v", key, err)
		}

		if len(val) > length {
			log.Fatalf("%v is too long", key)
		}

		for len(val) < length {
			if _, ok := k.Tag.Lookup("from"); ok == true {
				val = " " + val
			} else {
				val = val + " "
			}
		}
		return val
	}
	return ""
}

func (s station) string() string {
	t := reflect.TypeOf(station{})

	mainID := findKeyLen("MainID", s.MainID, t)

	location := findKeyLen("Location", s.Location, t)

	return fmt.Sprintf("%v%v", mainID, location)
}

func createDirectory(stationName string) error {
	if err := os.Mkdir(stationName, os.ModePerm); err != nil {
		return fmt.Errorf("creating directory: %v", err)
	}
	return nil
}

// CreateStationFile creates the file
func CreateStationFile(stationName string) {
	st := []station{
		station{MainID: "123", Location: "X"},
		station{MainID: "11", Location: "fives"},
	}

	if err := createDirectory(stationName); err != nil {
		log.Fatalf("creating directory: %v", err)
	}

	f, err := os.Create(stationName + "/file.ctl")
	if err != nil {
		log.Fatalf("creating file: %v", err)
	}
	defer f.Close()

	for _, s := range st {
		_, err = f.WriteString(s.string() + "\n")
		if err != nil {
			log.Fatalf("writing file: %v", err)
		}
	}
	f.Sync()
}
