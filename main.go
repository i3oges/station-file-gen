package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/i3oges/station-file-gen/station"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("\nStation Thing")
	fmt.Println("-------------")

	stationName, err := reader.ReadString('\n')
	if err != nil {
		log.Fatalf("error reading stdin: %v", err)
	}
	stationName = strings.Replace(stationName, "\n", "", 1)
	fmt.Println(stationName)
	station.CreateStationFile(stationName)
}
